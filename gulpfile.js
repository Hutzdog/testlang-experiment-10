const gulp = require('gulp'),
 git = require('gulp-git'),
 mocha = require('gulp-mocha'),
 readline = require('readline-sync')
 ;

 //Run the test suite
function test(cb) {
    gulp.src('test/*', {read: false})
    .pipe(mocha());

    cb();
}

//Push to the development branch
{
    let message = "Auto-generated commit message"; //Commit message
    let source = gulp.src('./*'); //Gulp source

    //Setup for this block
    function setupDev(cb) {
        let proceed = readline.question('Do you wish to continue(Y/n): ');
        if(proceed === 'n') process.exit();
        else {
            let l_message = readline.question('Please enter a commit message: ');
            if(l_message !== '') message = l_message;
            cb();
        }
    }

    //Stage and commit all changes
    function commitDev(cb) {
        source.pipe(git.add({args: '-A'})).pipe(git.commit(message));
        cb();
    }

    //Pushes changes to Dev branch
    function pushDev(cb) {
        git.push('origin', 'Dev', function (err) { if(err) throw err;});
        cb();
    }
}

exports.push = gulp.series(setupDev, commitDev, pushDev);

exports.test = test;