const assert = require('chai').assert;
describe("Lexer tests", function() {
    const lexer = require('../tl10lex').lexer;

    //Check all tokens
    describe("Token tests", function(){

        //Special symbols
        describe("Semicolons and symbols", function(){
            //Semicolons
            it("SEMICOLON tokens should be able to have values [;] and should return the type 'semicolon'.", function(){
                let x = lexer(';');
                assert.isDefined(x[0], "SEMICOLON token not stored. This should have been caught by the error handler unless the test suite has been tampered with.");
                assert.include(x[0], {type: 'semicolon'}, "The text ';' was improperly typed as: '" + x[0].type + "'. It should be type: 'semicolon'.");
                assert.include(x[0], {value: ';'}, "The text ';' was improperly stored as: '" + x[0].value + "'.");
            });

            //Parentheses
            it("PARENTHESES tokens should be able to have values [()] and should return the type 'paren'.", function(){
                let x = lexer('()');
                for(var i = 0; i < 2; i++) {
                    let curChar = '()'.charAt(i);
                    assert.isDefined(x[i], "PARENTHESES token not stored. This should have been caught by the error handler unless the test suite has been tampered with.");
                    assert.include(x[i], {type: 'paren'}, "The text " + curChar + " was improperly typed as: '" + x[i].type + "'. It should be type: 'paren'.");
                    assert.include(x[i], {value: curChar}, "The text " + curChar + " was improperly stored as: '" + x[i].value + "'.");
                }
            });
        });
        //Data types
        describe("Data Types", function(){
            //Integers
            it("INTEGER tokens should be able to have values '[0-9]+' and return the type 'int'.", function(){
                let x = lexer('10');
                assert.isDefined(x[0], "INTEGER token not stored. This should have been caught by the error handler unless the test suite has been tampered with.");
                assert.include(x[0], {type: 'int'}, "The number 10 was improperly typed as: '" + x[0].type + "'. It should be type: 'int'.");
                assert.include(x[0], {value: '10'}, "The number 10 was improperly stored as: '" + x[0].value + "'.");
            });

            //Floats
            it("FLOAT tokens should be able to have values '[0-9]+ .[0-9]+' and return the type 'float'.", function(){
                let x = lexer('10.2');
                assert.isDefined(x[0], "FLOAT token not stored. This should have been caught by the error handler unless the test suite has been tampered with.");
                assert.include(x[0], {type: 'float'}, "The number 10.2 was improperly typed as: '" + x[0].type + "'. It should be type: 'float'.");
                assert.include(x[0], {value: '10.2'}, "The number 10.2 was improperly stored as: '" + x[0].value + "'.");
            });

            //Test invalid number detection
            it("Check for numbers with more than 2 decimal points.", function(){
                assert.throws(function () {lexer('10.2.2')}, TypeError, "Invalid type: Too many decimal places");
            });

            //Strings
            it('STRING tokens should be able to hold ANY tokens, but must start with ' + "'" + '"' + "'. They return the type 'string'", function(){
                let x = lexer('"Test 12 )"');
                assert.isDefined(x[0], "STRING token not stored. This should have been caught by the error handler unless the test suite has been tampered with.");
                assert.isUndefined(x[1], "String failed to contain test message.");
                assert.include(x[0], {type: 'string'}, 'The test string "Test 12 )" was improperly typed as: ' + "'" + x[0].type + "'. It should be type: 'string'");
                assert.include(x[0], {value: 'Test 12 )'}, 'The test string "Test 12 )" was improperly stored as: ' + '"' + x[0].value + '".');
            });
        });
        describe("Other", function(){
            it('NAME tokens are all lowercase letters and have a type of name.', function() {
                let x = lexer('print');
                assert.isDefined(x[0], "NAME token not stored. This should have been caught by the error handler unless the test suite has been tampered with.")
                assert.include(x[0], {type: 'identifier'}, 'The test string "print" was improperly typed as: ' + "'" + x[0].type + "'. It should be type: 'identifier'");
                assert.include(x[0], {value: 'print'}, 'The test string "print" was improperly stored as: ' + '"' + x[0].value + '".');
            });

            //Ensure whitespace is never stored
            it("Make sure all whitespace is voided.", function(){
                let x = lexer('\n\r\t');
                assert.isUndefined(x[0], "Whitespace character stored: this should be imposssible.");
            });
        });        
    });
});