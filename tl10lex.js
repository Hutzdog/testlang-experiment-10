/*
  TTTTTTTTT*EEEEEEEEE   SSSSS  TTTTTTTTT
    T:::T   EE         SS        T:::T
    T:::T   EE          SS       T:::T
    T:::T   EEEEEEEEE    SS      T:::T
    T:::T   EE            SS     T:::T
    T:::T   EE             SS    T:::T
    TTTTT   EEEEEEEEE   SSSS     TTTTT

    Compiler 10
*/

class Token {
    constructor(values) {
        this.values = values;
    }

    compareValues(char) {
        return(this.values.test(char));
    }
}

//Tokens
SEMICOLON         = new Token(/[;]/);
PARENTHESES       = new Token(/[\(\)]/);
WHITESPACE        = new Token(/[\s\n\r\t]/);

DIGITS            = new Token(/[0-9]/);
STRING            = new Token(/["]/);

NAME              = new Token(/[a-z]/);

//Lexer
function lexer(p_input) {
    let input = p_input + ' '; //Input with infinite loop prevention
    let pos = 0; //Current position in code
    let toks = []; //List of tokens

    while (pos < input.length) {
        let cur = input[pos]; //Current character
        
        //Semicolons
        if(SEMICOLON.compareValues(cur)) {
            toks.push({type: 'semicolon', value: ';'});
            pos++;
            continue;
        }
        
        //Parentheses
        else if(PARENTHESES.compareValues(cur)) {
            toks.push({type: 'paren', value: cur});            pos++;
            continue;
        }

        //Whitespace
        else if(WHITESPACE.compareValues(cur)) {
            pos++;
            continue;
        }

        //Numbers
        else if(DIGITS.compareValues(cur)) {
            let l_value = ''; //Value for this statement
            let l_decCount = 0; //Number of decimal digits, used for typing and error handling

            while(DIGITS.compareValues(cur) | /[.]/.test(cur)) {
                if(/[.]/.test(cur)) l_decCount++;
                l_value += cur;
                cur = input[++pos];
            }
            switch(l_decCount) {
                case 0:
                    toks.push({type: 'int', value: l_value});
                    break;
                case 1:
                    toks.push({type: 'float', value: l_value});
                    break;
                default:
                    throw new TypeError('Invalid type: Too many decimal places');
            }
            pos++;
            continue;
        }

        //Strings
        else if(STRING.compareValues(cur)) {
            let l_value = '';  //Value for the string

            cur = input[++pos];

            while (!STRING.compareValues(cur)) {
                l_value += cur;
                cur = input[++pos];
            }

            cur = input[++pos];
            toks.push({type: 'string', value: l_value});

            continue;
        }

        //Keywords
        else if(NAME.compareValues(cur)) {
            let l_value = ''; //Value of the identifier

            while(NAME.compareValues(cur)) {
                l_value += cur;
                cur = input[++pos];
            }

            toks.push({type: 'identifier', value: l_value});
        }

        else throw new TypeError('Invalid token: ' + cur);
    }

    return toks;
}

module.exports.lexer = function (input) {
    return lexer(input);
}